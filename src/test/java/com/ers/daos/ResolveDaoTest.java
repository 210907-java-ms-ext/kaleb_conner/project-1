package com.ers.daos;
import com.ers.models.Request;
import com.ers.models.User;
import static org.junit.jupiter.api.Assertions.*;

import com.ers.services.ConnectionService;
import com.ers.servlets.RequestServlet;
import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ResolveDaoTest
{
    private User normal = new User(1, "John_Smith", false);
    private User manager = new User(3, "Aston_Jane_Holcomb", true);

    @BeforeAll
    public void createPendingRequest()
    {
        Request request = new Request(1, "test_app1234", 34657);
        request.submitRequest();
    }

    @AfterAll
    public void cleanup()
    {
        ConnectionService connectionService = new ConnectionService();
        try (Connection c = connectionService.establishConnection())
        {
            String sql = "delete from request where description='test_app1234' and value=34657";
            PreparedStatement pstmt = c.prepareStatement(sql);
            pstmt.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Test
    public void testPendingRequests()
    {
        ResolveDao dao = new ResolveDao(normal);
        ArrayList<Request> requests = dao.getPendingRequests();
        if (requests == null)
        {
            fail("failed to get pending requests");
        }
    }
}
