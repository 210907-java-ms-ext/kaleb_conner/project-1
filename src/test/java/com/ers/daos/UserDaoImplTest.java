package com.ers.daos;

import com.ers.models.User;
import org.junit.Test;

public class UserDaoImplTest
{
    private UserDaoImpl dao = new UserDaoImpl();

    @Test
    public void knownUserId()
    {
        User actual = dao.getUserById(1);
        User expected = new User(1, "John_Smith", false);
    }


    @Test
    public void knownUsername()
    {
        User actual = dao.getUserByUsername("John_Smith");
        User expected = new User(1, "John_Smith", false);
    }
}
