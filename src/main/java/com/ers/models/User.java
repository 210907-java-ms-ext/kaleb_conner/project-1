package com.ers.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Objects;

public class User implements Serializable
{
    private static final long serialVersionUID = 1L;

    private int id;
    private String username;
    private String firstName;
    private String lastName;

    @JsonIgnore
    private byte[] password;
    private boolean is_manager;

    public User() { super(); }

    /**
     * Gets the full user credentials, and counts as being "logged in"
     * @param Id user id
     * @param Username username
     * @param FirstName first name
     * @param LastName last name
     * @param Password hashed password
     * @param IsManager is a manager
     */
    public User(int Id, String Username, String FirstName, String LastName, byte[] Password, boolean IsManager)
    {
        super();
        id = Id;
        username = Username;
        firstName = FirstName;
        lastName = LastName;
        password = Password;
        is_manager = IsManager;
    }

    /**
     * Gets partial user credentials, only verifies that the user exists
     * @param Id user id
     * @param Username username
     */
    public User(int Id, String Username, boolean Is_manager)
    {
        id = Id;
        username = Username;
        firstName = null;
        lastName = null;
        password = null;
        is_manager = Is_manager;
    }

    public String getFullName()
    {
        return getFirstName() + " " + getLastName();
    }

    public boolean isLoggedIn()
    {
        return password != null;
    }

    public int getId()
    {
        return id;
    }

    public String getUsername()
    {
        return username;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public boolean is_manager()
    {
        return is_manager;
    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id);
    }
}
