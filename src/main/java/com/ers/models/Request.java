package com.ers.models;

import com.ers.daos.UserDaoImpl;
import com.ers.services.ConnectionService;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Request implements Serializable
{

    private int request_id;
    private int user_id;
    private String first_name = "";
    private String last_name = "";
    private String description;
    private int value;
    private ResolveState resolveState;
    private String resolvedBy;

    @JsonIgnore
    private static final ConnectionService connectionService = new ConnectionService();

    public Request(int Req_id, int User_id, String Description, int Value, int Resolve, int ResolvedBy)
    {
        request_id = Req_id;
        user_id = User_id;
        description = Description;
        value = Value;
        resolveState = ResolveState.values()[Resolve];
        resolvedBy = getResolvedByName(ResolvedBy);
    }

    public Request(int Req_id, int User_id, String First_name, String Last_name, String Description, int Value, int Resolve, int ResolvedBy)
    {
        request_id = Req_id;
        user_id = User_id;
        first_name = First_name;
        last_name = Last_name;
        description = Description;
        value = Value;
        resolveState = ResolveState.values()[Resolve];
        resolvedBy = getResolvedByName(ResolvedBy);
    }

    public Request(int User_id, String Description, int Value)
    {
        request_id = -1;
        user_id = User_id;
        description = Description;
        value = Value;
        resolveState = ResolveState.values()[0];
        resolvedBy = null;
    }

    public String getResolvedByName(int resolvedBy)
    {
        User user = new UserDaoImpl().getUserById(resolvedBy);
        return user.getFullName();
    }

    public boolean submitRequest()
    {
        String sql = "insert into request (user_id, description, value, resolve_state) values (?, ?, ?, 0)";
        try(Connection c = connectionService.establishConnection())
        {
            PreparedStatement pstmt = c.prepareStatement(sql);
            pstmt.setInt(1, user_id);
            pstmt.setString(2, description);
            pstmt.setInt(3, value);
            pstmt.execute();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
        return true;
    }

    public static boolean updateRequest(int state, int user_id, int request_id)
    {
        String sql = "update request set resolve_state = ?, resolve_by = ? where request_id = ?";

        try (Connection c = connectionService.establishConnection())
        {
            PreparedStatement pstmt = c.prepareStatement(sql);
            pstmt.setInt(1, state);
            pstmt.setInt(2, user_id);
            pstmt.setInt(3, request_id);
            pstmt.execute();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
        return true;
    }


    @Override
    public String toString()
    {
        return "ID: " + request_id +
                ", user_id:" + user_id +
                ", first_name: " + first_name +
                ", last_name: " + last_name +
                ", description:" + description +
                ", amount:" + value +
                ", " + resolveState +
                ", resolvedBy: " + resolvedBy;
    }

    public int getRequest_id()
    {
        return request_id;
    }

    public String getDescription()
    {
        return description;
    }

    public String getResolvedBy()
    {
        return resolvedBy;
    }

    public ResolveState getResolveState()
    {
        return resolveState;
    }

    public String getLast_name()
    {
        return last_name;
    }

    public String getFirst_name()
    {
        return first_name;
    }

    public int getValue()
    {
        return value;
    }

    public int getUser_id()
    {
        return user_id;
    }
}
