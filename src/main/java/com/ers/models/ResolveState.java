package com.ers.models;

public enum ResolveState
{
    pending,
    accepted,
    rejected
}
