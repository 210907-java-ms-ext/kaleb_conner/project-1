package com.ers.services;

public class AuthService
{
    private UserService userService = new UserService();

    /**
     * The auth token is a colon separated string which contains the user id and their authorization
     * @param authToken should contain a colon separated string
     * @return
     */
    public boolean validateToken(String authToken)
    {
        // we don't want blank tokens
        if (authToken == null)
        {
            return false;
        }

        // make sure the token is exactly a pair separated by a :
        String[] tokenArr = authToken.split(":");
        if(tokenArr.length != 2)
        {
            return false;
        }

        // then we take a look at the first value, making sure it's numeric
        String idString = tokenArr[0];
        if(!idString.matches("^\\d+$")){ // if it does not match a number regular expression, we return false
            return false;
        }
        // todo: change auth token to include password hash for verification?
        // currently just return if the user id exists
        return userService.getUser(Integer.getInteger(idString)) != null;
    }
}