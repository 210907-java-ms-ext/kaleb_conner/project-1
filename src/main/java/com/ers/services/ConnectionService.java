package com.ers.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionService {

    public Connection establishConnection() throws SQLException {

        //registering our JDBC driver in the classpath
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e){
            e.printStackTrace();
        }
        String url;
        String host = System.getenv("DBURL");
        if(host==null){
            url = "jdbc:postgresql://localhost:5432/postgres";
        } else {
            url = "jdbc:postgresql://"+host+":5432/postgres";
        }

        String username = System.getenv("DBUsername");
        String password = System.getenv("DBPassword");
        return DriverManager.getConnection(url, username, password);
    }

}
