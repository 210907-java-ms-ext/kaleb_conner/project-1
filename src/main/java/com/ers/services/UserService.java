package com.ers.services;

import com.ers.daos.UserDao;
import com.ers.daos.UserDaoImpl;
import com.ers.models.User;

public class UserService
{
    private UserDao userDao = new UserDaoImpl();

    public User getUser(int id)
    {
        return userDao.getUserById(id);
    }

    public User getUser(String username)
    {
        return userDao.getUserByUsername(username);
    }

    public User getUserByCredentials(String username, String password)
    {
        return userDao.getUserByUsernameAndPassword(username, password);
    }
}
