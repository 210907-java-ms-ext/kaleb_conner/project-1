package com.ers.servlets;

import com.ers.daos.ResolveDao;
import com.ers.daos.UserDao;
import com.ers.daos.UserDaoImpl;
import com.ers.models.Request;
import com.ers.models.ResolveState;
import com.ers.models.User;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class RequestServlet extends HttpServlet
{
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException
    {
        String descriptionParam = req.getParameter("description");
        if (descriptionParam == null)
        {
            String type = req.getParameter("type");
            if (type == null)
            {
                resp.sendError(400, "Request is malformed.");
            }
            else
            {
                int reqId = Integer.parseInt(req.getParameter("reqId"));
                int state;
                if (type.equals("rejected"))
                {
                    state = 2;
                }
                else
                {
                    state = 1;
                }
                int userId = Integer.parseInt(req.getParameter("userId"));

                if (!Request.updateRequest(state, userId, reqId))
                {
                    resp.sendError(401, "Invalid Request ID");
                }
                else
                {
                    resp.setStatus(200);
                }
            }
        }
        else
        {
            String amountStr = req.getParameter("amount");
            int amountParam = Integer.parseInt(amountStr);
            int userId = Integer.parseInt(req.getParameter("user_id"));
            System.out.println("Request received from " + userId + " : " + descriptionParam + " " + amountParam);


            Request request = new Request(userId, descriptionParam, amountParam);
            if(!request.submitRequest()){
                resp.sendError(401, "User credentials provided did not return a valid account");
            } else {
                resp.setStatus(200);
            }
        }
    }


    // expects a request of either pending or completed requests
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException
    {
        String userIdStr = req.getParameter("id");
        int userId = Integer.parseInt(userIdStr);
        String type = req.getParameter("type");
        ArrayList<Request> requests = null;

        ResolveDao resolve = new ResolveDao(new UserDaoImpl().getUserById(userId));
        switch (type) {
            case "pending":
                System.out.println("Request for pending requests received from " + userId);
                requests = resolve.getPendingRequests();
                break;
            case "completed":
                System.out.println("Request for completed results received from " + userId);
                requests = resolve.getResolvedRequests();
                break;
            case "allpending":
                System.out.println("Request for all pending requests received from " + userId);
                requests = resolve.getAllPendingRequests();
                break;
            case "allcompleted":
                System.out.println("Request for all requests received from " + userId);
                requests = resolve.getAllRequests();
                break;
        }

        if(requests == null){
            resp.sendError(401, "User credentials provided did not return a valid account");
        } else {
            try (PrintWriter pw = resp.getWriter())
            {
                ObjectMapper om = new ObjectMapper();
                String json = om.writeValueAsString(requests);
                System.out.println(json);
                pw.write(json);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            resp.setStatus(200);
        }
    }

}
