package com.ers.servlets;

import com.ers.daos.ResolveDao;
import com.ers.models.Request;
import com.ers.models.User;
import com.ers.services.AuthService;
import com.ers.services.UserService;

import com.fasterxml.jackson.databind.ObjectMapper;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class UserServlet extends HttpServlet
{

    private AuthService authService = new AuthService();
    private UserService userService = new UserService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        // look at token - check if the appropriate role is present
        String authToken = req.getHeader("Authorization");

        boolean tokenIsValidFormat = authService.validateToken(authToken);
        if(!tokenIsValidFormat)
        {
            resp.sendError(400, "Improper token format");
        }
        else
        {
            int id = 0;
            try
            {
                id = Integer.parseInt(authToken.split(":")[0]);
            }
            catch (Exception e)
            {
                resp.sendError(400, "Improper token format");
                return;
            }
            User currentUser = userService.getUser(id);

            if(currentUser==null)
            {
                resp.sendError(401, "Auth token invalid - no user");
            }
            else
            {
                // this is technically a security hole
                // we only care if the user id is valid for now
                resp.setStatus(200);
                try(PrintWriter pw = resp.getWriter())
                {
                    // write to response body
                    ObjectMapper om = new ObjectMapper(); // ObjectMapper utilizes Java Reflection to introspect
                    // on the user object to create my json
                    ArrayList<Request> requests = new ResolveDao(currentUser).getPendingRequests();
                    String userJson = om.writeValueAsString(requests);
                    pw.write(userJson);
                }
            }
        }
    }
}
