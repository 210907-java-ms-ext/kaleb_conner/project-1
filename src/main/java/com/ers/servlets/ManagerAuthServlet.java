package com.ers.servlets;

import com.ers.models.User;
import com.ers.services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ManagerAuthServlet extends HttpServlet
{

    private UserService userService = new UserService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException
    {
        // get username and password from request
        // because we're using .getParameter with a post request, our servlet is expecting
        // key value pairs in the request body, along with the application/x-www-formurlencoded content type
        String usernameParam = req.getParameter("username");
        String passwordParam = req.getParameter("password");
//        String idStringParam = req.getParameter("id"); // params not present will be null
        System.out.println("Credentials received (management): "+usernameParam +" "+passwordParam);


        // check to see if user/pass match a user in the db
        User user = userService.getUserByCredentials(usernameParam, passwordParam);

        // sent 401 (Unauthorized) if we can't find a user with those credentials
        if(user == null){
            resp.sendError(401, "User credentials provided did not return a valid account");
        } else if (!user.is_manager())
        {
            System.out.println("This user is not a manager");
            resp.sendError(401, "User does not have manager credentials");
        }
        else {
            // send 200 (OK) if we do find a user with those credentials
            resp.setStatus(200);
            // we can also send back some token that identifies the particular user that matched
            String token = user.getId() + ":" + user.is_manager();
            resp.setHeader("Authorization", token);
        }
    }
}
