package com.ers.daos;

import com.ers.models.User;
import com.ers.services.ConnectionService;
import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDao
{
    private final ConnectionService connectionService = new ConnectionService();


    public User getUserById(int id)
    {
        String sql = "select * from account where user_id = ?";
        try (Connection c = connectionService.establishConnection()
        )
        {
            PreparedStatement pstmt = c.prepareStatement(sql);
            pstmt.setInt(1, id);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next())
            {
                return new User(rs.getInt("user_id"),
                        rs.getString("user_name"),
                        rs.getBoolean("is_manager"));
            }
        }
        catch (SQLException throwables) { System.out.println("There was an unexpected error when connecting to the database."); }
        return null;
    }

    public User getUserByUsername(String username)
    {
        String sql = "select * from account where user_name = ?";
        try (Connection c = connectionService.establishConnection()
        )
        {
            PreparedStatement pstmt = c.prepareStatement(sql);
            pstmt.setString(1, username);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next())
            {
                return new User(rs.getInt("user_id"),
                        rs.getString("user_name"),
                        rs.getBoolean("is_manager"));
            }
        }
        catch (SQLException throwables) { System.out.println("There was an unexpected error when connecting to the database."); }
        return null;
    }


    /**
     * Generates the password hash. It is ambiguous enough to safely replace with a different hash function.
     * @param password password to convert
     * @return sha-256 hash of the password
     */
    // suppresses the beta warning
    @SuppressWarnings("UnstableApiUsage")
    public static byte[] passHash(String password)
    {
        // return password;
        return Hashing.sha256().hashString(password, StandardCharsets.UTF_8).asBytes();
    }

    /**
     * Checks that the username and password combination are valid
     * @param username account username
     * @param password account password
     * @return Selected user credentials
     */
    public User getUserByUsernameAndPassword(String username, String password)
    {
        String sql = "select * from account where user_name = ? and pass_hash = ?";
        try (Connection c = connectionService.establishConnection()
        )
        {
            PreparedStatement pstmt = c.prepareStatement(sql);
            pstmt.setString(1, username);
            pstmt.setBytes(2, passHash(password));
            ResultSet rs = pstmt.executeQuery();
            if (rs.next())
            {
                return new User(rs.getInt("user_id"),
                        rs.getString("user_name"),
                        rs.getString("first_name"),
                        rs.getString("last_name"),
                        rs.getBytes("pass_hash"),
                        rs.getBoolean("is_manager"));
            }
        }
        catch (SQLException throwables)
        {
            throwables.printStackTrace();
            System.out.println("There was an unexpected error when connecting to the database.");
        }
        return null;
    }

}
