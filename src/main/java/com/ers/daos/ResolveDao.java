package com.ers.daos;

import com.ers.models.Request;
import com.ers.models.User;
import com.ers.services.ConnectionService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ResolveDao
{
    private final ConnectionService connectionService = new ConnectionService();

    private int user_id;
    private String username;
    private boolean isManager;

    public ResolveDao(User user)
    {
        user_id = user.getId();
        username = user.getUsername();
        isManager = user.is_manager();
    }


    /**
     * gets the users own pending requests
     * @return the list of requests
     */
    public ArrayList<Request> getPendingRequests()
    {
        ArrayList<Request> requests = new ArrayList<>();
        String sql = "select * from request where user_id = ? and resolve_state = 0";

        try (Connection c = connectionService.establishConnection())
        {
            PreparedStatement pstmt = c.prepareStatement(sql);
            pstmt.setInt(1, user_id);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next())
            {
                requests.add(new Request(rs.getInt("request_id"),
                                         user_id,
                                         rs.getString("description"),
                                         rs.getInt("value"),
                                         rs.getInt("resolve_state"),
                                         rs.getInt("resolve_by")));
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return requests;
    }

    public ArrayList<Request> getAllPendingRequests()
    {
        ArrayList<Request> requests = new ArrayList<>();
        if(!isManager)
        {
            return null;
        }
        String sql = "select * from account " +
                "inner join request on account.user_id =request.user_id " +
                "left join resolve_state on request.resolve_state = resolve_state.id " +
                "where request.resolve_state = 0; ";

        try (Connection c = connectionService.establishConnection())
        {
            PreparedStatement pstmt = c.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next())
            {
                requests.add(new Request(rs.getInt("request_id"),
                        user_id,
                        rs.getString("first_name"),
                        rs.getString("last_name"),
                        rs.getString("description"),
                        rs.getInt("value"),
                        rs.getInt("resolve_state"),
                        rs.getInt("resolve_by")));
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return requests;
    }


    public ArrayList<Request> getResolvedRequests()
    {
        ArrayList<Request> requests = new ArrayList<>();
        String sql = "select * from request where user_id = ? and resolve_state != 0";

        try (Connection c = connectionService.establishConnection())
        {
            PreparedStatement pstmt = c.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next())
            {
                requests.add(new Request(rs.getInt("request_id"),
                        user_id,
                        rs.getString("description"),
                        rs.getInt("value"),
                        rs.getInt("resolve_state"),
                        rs.getInt("resolve_by")));
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return requests;
    }

    public ArrayList<Request> getAllRequests()
    {
        ArrayList<Request> requests = new ArrayList<>();
        if(!isManager)
        {
            return null;
        }

        String sql = "select * from account " +
                "inner join request on account.user_id = request.user_id " +
                "left join resolve_state on request.resolve_state = resolve_state.id;";

        try (Connection c = connectionService.establishConnection())
        {
            PreparedStatement pstmt = c.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next())
            {
                requests.add(new Request(rs.getInt("request_id"),
                        user_id,
                        rs.getString("description"),
                        rs.getInt("value"),
                        rs.getInt("resolve_state"),
                        rs.getInt("resolve_by")));
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return requests;
    }
}
