package com.ers.daos;

import java.util.List;
import com.ers.models.User;

public interface UserDao
{
    User getUserById(int id);
    User getUserByUsername(String username);
    User getUserByUsernameAndPassword(String username, String password);
}
