// listen for the login button click
document.getElementById("login-btn").addEventListener("click", attemptLogin);


function attemptLogin()
{
    const errorDiv = document.getElementById("error-div");
    errorDiv.hidden = true;

    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;

    console.log(`username: ${username}, password ${password}`);

    const xhr = new XMLHttpRequest();
    xhr.open("POST","http://ec2-3-140-252-225.us-east-2.compute.amazonaws.com:8082/Project-1-1.0/login");

    xhr.onreadystatechange = function()
    {
        if(xhr.status===401)
        {
            errorDiv.hidden = false;
            errorDiv.innerText = "invalid credentials";
        }
        else if (xhr.status===200)
        {
            const token = xhr.getResponseHeader("Authorization");
            console.log(token);
            sessionStorage.setItem("token", token);
            window.location.href="http://ec2-3-140-252-225.us-east-2.compute.amazonaws.com:8082/Project-1-1.0/home.html";
        }
        else
        {
            errorDiv.hidden = false;
            errorDiv.innerText = "unknown error";
        }
    }

    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    const requestBody = `username=${username}&password=${password}`;
    xhr.send(requestBody);
}
