document.getElementById("submit-btn").addEventListener("click", attemptSubmit);
document.getElementById("viewPR").addEventListener("click", request);
document.getElementById("viewPR").theType = "pending";
document.getElementById("viewCR").addEventListener("click", request);
document.getElementById("viewCR").theType = "completed";
document.getElementById("signOut").addEventListener("click", signOut);

const token = sessionStorage.getItem("token");

function signOut()
{
    sessionStorage.removeItem("token");
    window.location.href = "http://ec2-3-140-252-225.us-east-2.compute.amazonaws.com:8082/Project-1-1.0/login.html"
}

function request(evt)
{
    const errorDiv = document.getElementById("main-content");

    const xhr = new XMLHttpRequest();
    const id = sessionStorage.getItem("token").split(":")[0]
    const type = evt.currentTarget.theType;
    xhr.open("GET","request?id="+id+"&type="+type);

    xhr.onload = function()
    {
        if (xhr.status===200)
        {
            let requests = JSON.parse(xhr.responseText)
            createRequestsTable(requests);
        }
    }
    xhr.send();
}

function createRequestsTable(requestList)
{
    let requestTable = document.querySelector("#reqtable");
    while (requestTable.firstChild)
    {
        requestTable.removeChild(requestTable.firstChild);
    }

    for (let request of requestList)
    {
        let row = document.createElement("tr");

        let requestId = document.createElement("td");
        requestId.innerHTML = request.request_id;
        row.appendChild(requestId);

        let description = document.createElement("td");
        description.innerHTML = request.description;
        row.appendChild(description);

        let resolveState = document.createElement("td");
        resolveState.innerHTML = request.resolveState;
        row.appendChild(resolveState);

        let resolvedBy = document.createElement("td");
        resolvedBy.innerHTML = request.resolvedBy;
        row.appendChild(resolvedBy);

        requestTable.appendChild(row);
    }
}


function attemptSubmit()
{
    const errorDiv = document.getElementById("error-div");
    errorDiv.hidden = true;

    const description = document.getElementById("description").value;
    const amount = document.getElementById("amount").value;

    console.log(`description: ${description}, amount: ${amount}`);

    const xhr = new XMLHttpRequest();
    const token = sessionStorage.getItem("token");
    let id = token.split(":")[0];
    xhr.open("POST", "request/description="+description+"&amount="+amount+"&user_id="+id);

    xhr.onreadystatechange = function()
    {
        if(xhr.status===401)
        {
            errorDiv.hidden = false;
            errorDiv.innerText = "An error occurred";
        }
        else if (xhr.status===200)
        {
            window.alert("Your request has been received. It will be processed by a manager soon.");
            window.location.href="http://ec2-3-140-252-225.us-east-2.compute.amazonaws.com:8082/Project-1-1.0/home.html";
        }
        else
        {
            errorDiv.hidden = false;
            errorDiv.innerText = "unknown error";
        }
    }
    xhr.send();
}


if (!token)
{
    window.location.href = "http://http://ec2-3-140-252-225.us-east-2.compute.amazonaws.com:8082/Project-1-1.0/login.html"
}