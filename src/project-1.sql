create table account
(
	user_id serial primary key,
	user_name varchar(32),
	first_name varchar(16),
	last_name varchar(16),
	pass_hash bytea,
	is_manager boolean
);

create table resolve_state
(
	id serial primary key,
	state varchar(16)
);

insert into resolve_state (id, state) values
	(0, 'not resolved'),
	(1, 'accepted'),
	(2, 'rejected');


create table request
(
	request_id serial primary key,
	user_id serial,
	foreign key(user_id) references account(user_id),
	description varchar(30),
	value serial,
	resolve_state serial,
	foreign key(resolve_state) references resolve_state(id),
	resolve_by serial,
	foreign key(resolve_by) references account(user_id)
);